require 'date'

class SessionMapper
  attr_accessor :new_times, :booked_times, :available_times, :suspended_times, :result

  def initialize(old_times, new_times)
    @new_times = new_times
    @booked_times = old_times.select { |time| time[:state] == 'booked' }
    @available_times = old_times.select { |time| time[:state] == 'available' }
    @suspended_times = old_times.select { |time| time[:state] == 'suspended' }
    @result = {}
    @matches = {}
  end

  private_class_method :new

  def self.call(old_times, new_times)
    new(old_times, new_times).process
  end

  def process
    handle_booked_times
    handle_available_times
    handle_suspended_times
    @result
  end

  private

  def handle_booked_times
    calculate_booked_times_matches
    process_booked_times
  end

  def handle_available_times
    @result.merge!(@available_times.zip(@new_times).to_h)
  end

  def handle_suspended_times
    @suspended_times.each do |time|
      @result[time] = nil
    end
  end

  def calculate_booked_times_matches
    @booked_times.each do |booked_time|
      matches_for_time = @new_times.map do |new_time|
        Hash[new_time, calculate_timeslot_match(booked_time, new_time)]
      end
      @matches[booked_time] = matches_for_time
    end
  end

  def process_booked_times
    while @booked_times.any? do
      best_match = @matches.max_by do |_, match_array|
        match_array.map { |_, match| match }.max
      end

      matched_new_time = best_match[1].max_by { |match| match.values.max }.keys.first
      @result[best_match[0]] = matched_new_time

      @new_times.delete(matched_new_time)
      @matches.delete(best_match[0])
      @booked_times.delete(best_match[0])

      @matches.transform_values! { |match_array| match_array.select { |match| match.first[0] != matched_new_time } }
    end
  end

  def calculate_timeslot_match(old_time, new_time)
    old_time_start = DateTime.parse(old_time[:starts_at]).to_time
    old_time_end = DateTime.parse(old_time[:ends_at]).to_time
    new_time_start = DateTime.parse(new_time[:starts_at]).to_time
    new_time_end = DateTime.parse(new_time[:ends_at]).to_time

    [old_time_end, new_time_end].min - [old_time_start, new_time_start].max
  end
end
