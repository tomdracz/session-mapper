# SessionMapper

## Running the script
Run `ruby example.rb` in the repository root

## Running the test suite
Run `bundle exec rspec` in the repository root

## Environment info
Coded and tested against Ruby 2.6.5 on OSX

## Implementation
Following assumptions have been made while coding the solution:
- each `old_times` argument passed to `SessionMapper.call` will have 2 suspended sessions resulting in ability to map all available and booked timeslots to the new times
- suspended times should still be present in the `SessionMapper.call` result but they will map to `nil` values
- after processing booked times, available times can be distributed over remaining new times without need for close matching to times
- better time matches should be prioritised, even if it results in other session having to be matched to less-optimal time. Essentially the assumption is that it's better to have one good match and one mediocre match rather than two average matches
- `old_times` and `new_times` will both cover the same day, just different times

The bulk of implementation complexity lies in trying to find the best matches for booked times. The best matches are calculated here as time overlap (or lack of) between old timeslot and possible new timeslot.

The implementation first calculates new time matches for each of the old booked times. Best matches are then prioritised and processed first. Once each old time is processed, it's removed from the processing loop and then ew time that has been matched is also made unavailable to be associated with the old time in the further processing.

Once all booked times are processed, the available times are distributed over remaining new times but without any time matching consideration. Finally suspended times are matched to nil values.
