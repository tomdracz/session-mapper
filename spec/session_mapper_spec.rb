require_relative '../lib/session_mapper'

RSpec.describe "SessionMapper" do
  let(:old_times) do
    [
      {
        starts_at: '2021-06-23T09:00:00Z',
        ends_at: '2021-06-23T09:45:00Z',
        state: 'booked'
      },
      {
        starts_at: '2021-06-23T09:45:00Z',
        ends_at: '2021-06-23T10:30:00Z',
        state: 'suspended'
      },
      {
        starts_at: '2021-06-23T10:30:00Z',
        ends_at: '2021-06-23T11:15:00Z',
        state: 'booked'
      },
      {
        starts_at: '2021-06-23T11:15:00Z',
        ends_at: '2021-06-23T12:00:00Z',
        state: 'suspended'
      },
      {
        starts_at: '2021-06-23T13:30:00Z',
        ends_at: '2021-06-23T14:15:00Z',
        state: 'available'
      },
      {
        starts_at: '2021-06-23T14:15:00Z',
        ends_at: '2021-06-23T15:00:00Z',
        state: 'available'
      },
      {
        starts_at: '2021-06-23T15:00:00Z',
        ends_at: '2021-06-23T15:45:00Z',
        state: 'booked'
      },
      {
        starts_at: '2021-06-23T15:45:00Z',
        ends_at: '2021-06-23T16:30:00Z',
        state: 'booked'
      }
    ]
  end

  let(:new_times) do
    [
      {
        starts_at: '2021-06-23T09:00:00Z',
        ends_at: '2021-06-23T09:50:00Z'
      },
      {
        starts_at: '2021-06-23T10:00:00Z',
        ends_at: '2021-06-23T10:50:00Z'
      },
      {
        starts_at: '2021-06-23T11:00:00Z',
        ends_at: '2021-06-23T11:50:00Z'
      },
      {
        starts_at: '2021-06-23T13:00:00Z',
        ends_at: '2021-06-23T13:50:00Z'
      },
      {
        starts_at: '2021-06-23T14:00:00Z',
        ends_at: '2021-06-23T14:50:00Z'
      },
      {
        starts_at: '2021-06-23T15:00:00Z',
        ends_at: '2021-06-23T15:50:00Z'
      }
    ]
  end

  let(:session_mapper_instance) { SessionMapper.send(:new, old_times, new_times) }

  describe "::call" do
    let(:session_mapper_instance) { instance_double(SessionMapper) }

    it "creates a new instance of SessionMapper and dispatches call method, passing old and new times" do
      expect(SessionMapper).to receive(:new).with(old_times, new_times).and_return(session_mapper_instance)
      expect(session_mapper_instance).to receive(:process)

      SessionMapper.call(old_times, new_times)
    end
  end

  describe "#initialize" do
    it "sets the new times" do
      expect(session_mapper_instance.new_times.size).to eq(6)
      expect(session_mapper_instance.new_times).to eq(new_times)
    end

    it "sets the booked times" do
      expect(session_mapper_instance.booked_times.size).to eq(4)
      expect(session_mapper_instance.booked_times).to include(
        {
          starts_at: '2021-06-23T09:00:00Z',
          ends_at: '2021-06-23T09:45:00Z',
          state: 'booked'
        },
        {
          starts_at: '2021-06-23T10:30:00Z',
          ends_at: '2021-06-23T11:15:00Z',
          state: 'booked'
        },
        {
          starts_at: '2021-06-23T15:00:00Z',
          ends_at: '2021-06-23T15:45:00Z',
          state: 'booked'
        },
        {
          starts_at: '2021-06-23T15:45:00Z',
          ends_at: '2021-06-23T16:30:00Z',
          state: 'booked'
        }
      )
    end

    it "sets the available times" do
      expect(session_mapper_instance.available_times.size).to eq(2)
      expect(session_mapper_instance.available_times).to include(
        {
          starts_at: '2021-06-23T13:30:00Z',
          ends_at: '2021-06-23T14:15:00Z',
          state: 'available'
        },
        {
          starts_at: '2021-06-23T14:15:00Z',
          ends_at: '2021-06-23T15:00:00Z',
          state: 'available'
        }
      )
    end

    it "sets the suspended times" do
      expect(session_mapper_instance.suspended_times.size).to eq(2)
      expect(session_mapper_instance.suspended_times).to include(
        {
          starts_at: '2021-06-23T09:45:00Z',
          ends_at: '2021-06-23T10:30:00Z',
          state: 'suspended'
        },
        {
          starts_at: '2021-06-23T11:15:00Z',
          ends_at: '2021-06-23T12:00:00Z',
          state: 'suspended'
        }
      )
    end

    it "initializes result object with empty hash" do
      expect(session_mapper_instance.result).to eq({})
    end
  end

  describe "#process" do
    context "with only available and suspended times" do
      let(:old_times) do
        [
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          },
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          },
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'available'
          }
        ]  
      end

      it "matches the available times to new times and sets suspended times to nil" do
        expect(session_mapper_instance.process).to eq({
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'available'
          } =>  {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:50:00Z'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T10:00:00Z',
            ends_at: '2021-06-23T10:50:00Z'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T11:00:00Z',
            ends_at: '2021-06-23T11:50:00Z'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T13:00:00Z',
            ends_at: '2021-06-23T13:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T14:00:00Z',
            ends_at: '2021-06-23T14:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:50:00Z'
          }         
        })
      end
    end

    context "with one booked time" do
      let(:old_times) do
        [
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          },
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'booked'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          },
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'available'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'available'
          }
        ]  
      end

      it "matches the booked time to best matching slot and matches available times to remaining new times" do
        expect(session_mapper_instance.process).to eq({
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:50:00Z'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T10:00:00Z',
            ends_at: '2021-06-23T10:50:00Z'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T11:00:00Z',
            ends_at: '2021-06-23T11:50:00Z'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T13:00:00Z',
            ends_at: '2021-06-23T13:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T14:00:00Z',
            ends_at: '2021-06-23T14:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:50:00Z'
          }
        })
      end
    end

    context "with multiple booked times" do
      it "matches the booked times to best matching slots and matches available times to remaining new times" do
        expect(session_mapper_instance.process).to eq({
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:50:00Z'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T10:00:00Z',
            ends_at: '2021-06-23T10:50:00Z'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T11:00:00Z',
            ends_at: '2021-06-23T11:50:00Z'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'available'
          } => {
            starts_at: '2021-06-23T13:00:00Z',
            ends_at: '2021-06-23T13:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T14:00:00Z',
            ends_at: '2021-06-23T14:50:00Z'
          }
        })
      end
    end

    context "with only booked and suspended times" do
      let(:old_times) do
        [
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'booked'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          },
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'booked'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          },
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'booked'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'booked'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'booked'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'booked'
          }
        ]  
      end

      it "matches all the booked times to best matching slots" do
        expect(session_mapper_instance.process).to eq({
          {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:45:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T09:00:00Z',
            ends_at: '2021-06-23T09:50:00Z'
          },
          {
            starts_at: '2021-06-23T09:45:00Z',
            ends_at: '2021-06-23T10:30:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T10:30:00Z',
            ends_at: '2021-06-23T11:15:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T10:00:00Z',
            ends_at: '2021-06-23T10:50:00Z'
          },
          {
            starts_at: '2021-06-23T11:15:00Z',
            ends_at: '2021-06-23T12:00:00Z',
            state: 'suspended'
          } => nil,
          {
            starts_at: '2021-06-23T13:30:00Z',
            ends_at: '2021-06-23T14:15:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T13:00:00Z',
            ends_at: '2021-06-23T13:50:00Z'
          },
          {
            starts_at: '2021-06-23T14:15:00Z',
            ends_at: '2021-06-23T15:00:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T14:00:00Z',
            ends_at: '2021-06-23T14:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:45:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T15:00:00Z',
            ends_at: '2021-06-23T15:50:00Z'
          },
          {
            starts_at: '2021-06-23T15:45:00Z',
            ends_at: '2021-06-23T16:30:00Z',
            state: 'booked'
          } => {
            starts_at: '2021-06-23T11:00:00Z',
            ends_at: '2021-06-23T11:50:00Z'
          }
        })
      end
    end
  end
end
